package com.company;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class ServerSomething extends Thread {

    private Socket socket; // сокет, через который сервер общается с клиентом,
    private BufferedReader in; // поток чтения из сокета
    private BufferedWriter out; // поток записи в сокет
    ArrayList<String> PRODUCTS = new ArrayList<String>() {
        {
            add("Молоко");
            add("Хлеб");
            add("Конфеты");
        }
    };
    Pattern numberPattern = Pattern.compile("[0-9]+");
    Matcher numberMatcher;
    Pattern namePattern = Pattern.compile("\\s.+");
    Matcher nameMatcher;



    public ServerSomething(Socket socket) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        start(); // вызываем run()
    }
    @Override
    public void run() {
        String word;
        try {

            try {
                while (true) {
                    word = in.readLine();
                    if(word.equals("stop")) {
                        this.downService();
                        break;
                    }
                    System.out.println("Echo: " + word);
                    numberMatcher = numberPattern.matcher(word);
                    nameMatcher = namePattern.matcher(word);
                    String response = "";
                    if(word.equals("get_products")){
                        Server.productList.printProducts(out);
                    }
                    if ( word.indexOf("remove_product") != -1 && numberMatcher.find()) {
                        Integer index = Integer.parseInt(numberMatcher.group(0));
                        Server.productList.removeProduct(index.intValue());
                        response = "Продукт успешно удален!";
                        this.send(response);
                    }
                    if ( word.indexOf("add_product") != -1 && nameMatcher.find()) {
                        String newProduct = nameMatcher.group(0);
                        Server.productList.addProduct(newProduct);
                        response = "Продукт Был успешно добавлен!";
                        this.send(response);
                    }



                }
            } catch (NullPointerException ignored) {}


        } catch (IOException e) {
            this.downService();
        }
    }

    private void send(String msg) {
        try {
            out.write(msg + "\n");
            out.flush();
        } catch (IOException ignored) {}

    }


    private void downService() {
        try {
            if(!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
                for (ServerSomething vr : Server.serverList) {
                    if(vr.equals(this)) vr.interrupt();
                    Server.serverList.remove(this);
                }
            }
        } catch (IOException ignored) {}
    }
}

class Products {

    private ArrayList<String> Products = new ArrayList<String>() {
        {
            add("Молоко");
            add("Хлеб");
            add("Конфеты");
        }
    };

    /**
     * добавить новый элемент в список
     *
     * @param el
     */

    public void addProduct(String el) {
        Products.add(el);
    }

    public void printProducts(BufferedWriter writer) {
        if (Products.size() > 0) {
            try {
                String response = "";
                for (int i = 0; i < Products.size(); i++) {
                    response += i + " - " + Products.get(i) + "|";
                }
                writer.write(response + "\n");
                writer.flush();
            } catch (IOException ignored) {
            }

        }

    }
    public void removeProduct(int index) {
        if(index < Products.size()){
            Products.remove(index);
        }
    }
}

public class Server {

    public static final int PORT = 54321;
    public static LinkedList<ServerSomething> serverList = new LinkedList<>(); // список всех нитей - экземпляров
    // сервера, слушающих каждый своего клиента
    public static Products productList;


    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(PORT);
        productList = new Products();
        System.out.println("Server Started");
        try {
            while (true) {
                Socket socket = server.accept();
                try {
                    serverList.add(new ServerSomething(socket)); // добавить новое соединенние в список
                } catch (IOException e) {
                    socket.close();
                }
            }
        } finally {
            server.close();
        }
    }
}